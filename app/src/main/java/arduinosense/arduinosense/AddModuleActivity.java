package arduinosense.arduinosense;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import arduinosense.arduinosense.models.ButtonModule;
import arduinosense.arduinosense.models.ConsoleModule;
import arduinosense.arduinosense.models.Module;
import arduinosense.arduinosense.models.Project;
import arduinosense.arduinosense.models.SliderModule;
import arduinosense.arduinosense.utils.Utils;

//AddModuleActivity adds modules (Button, Slider, Console) to the ProjectActivity

public class AddModuleActivity extends AppCompatActivity {
    private Project project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_module);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.project = (Project) getIntent().getSerializableExtra("project");

        TextView io_button = (TextView) findViewById(R.id.io_module);
        TextView buttonModule_button = (TextView) findViewById(R.id.button_module);
        TextView slider_button = (TextView) findViewById(R.id.slider_module);
        TextView console_button = (TextView) findViewById(R.id.consule_module);

        io_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Module module = new Module(project.countModule(Module.class), Module.class, 0, Utils.OUTPUT);

                Intent intent = new Intent(getBaseContext(), EditModuleActivity.class);
                intent.putExtra("module", module);

                startActivityForResult(intent, 1);
            }
        });

        buttonModule_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonModule buttonModule = new ButtonModule(project.countModule(ButtonModule.class));

                Intent intent = new Intent(getBaseContext(), EditModuleActivity.class);
                intent.putExtra("module", buttonModule);

                startActivityForResult(intent, 1);
            }
        });

        slider_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SliderModule sliderModule = new SliderModule(project.countModule(SliderModule.class));

                Intent intent = new Intent(getBaseContext(), EditModuleActivity.class);
                intent.putExtra("module", sliderModule);
                startActivityForResult(intent, 1);
            }
        });

        console_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConsoleModule consoleModule = new ConsoleModule(project.countModule(ConsoleModule.class));

                Intent intent = new Intent(getBaseContext(), EditModuleActivity.class);
                intent.putExtra("module", consoleModule);
                startActivityForResult(intent, 1);
            }
        });
    }

    /**
     * Recieves our objects from the activity we started
     *
     * @param requestCode int
     * @param resultCode int
     * @param data Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if (resultCode == 1) {
                ButtonModule btn = (ButtonModule) data.getSerializableExtra("button");
                this.project.addModule(btn);
                this.project.save();
                Intent intent = new Intent();
                intent.putExtra("button", btn);
                intent.putExtra("project", project);
                if (getParent() == null) {
                    setResult(1, intent);
                } else {
                    getParent().setResult(1, intent);
                }

                finish();
            }

            if (resultCode == 2) {
                SliderModule slider = (SliderModule) data.getSerializableExtra("slider");
                this.project.addModule(slider);
                this.project.save();
                Intent intent = new Intent();
                intent.putExtra("slider", slider);
                intent.putExtra("project", project);
                if (getParent() == null) {
                    setResult(2, intent);
                } else {
                    getParent().setResult(2, intent);
                }

                finish();
            }

            if (resultCode == 3) {
                ConsoleModule console = (ConsoleModule) data.getSerializableExtra("console");
                this.project.addModule(console);
                this.project.save();
                Intent intent = new Intent();
                intent.putExtra("console", console);
                intent.putExtra("project", project);
                if (getParent() == null) {
                    setResult(3, intent);
                } else {
                    getParent().setResult(3, intent);
                }

                finish();
            }

            if (resultCode == 4) {
                Module module = (Module) data.getSerializableExtra("module");
                this.project.addModule(module);
                this.project.save();
                Intent intent = new Intent();
                intent.putExtra("module", module);
                intent.putExtra("project", project);
                if (getParent() == null) {
                    setResult(4, intent);
                } else {
                    getParent().setResult(4, intent);
                }

                finish();
            }
        }
    }
}
