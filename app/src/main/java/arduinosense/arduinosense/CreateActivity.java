package arduinosense.arduinosense;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.models.Project;

public class CreateActivity extends AppCompatActivity {
    private BluetoothDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_create);

        Button bt_button = (Button) findViewById(R.id.linkDeviceButton);
        Button save_button = (Button) findViewById(R.id.saveSettingsButton);

        bt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(CreateActivity.this, BluetoothSetup.class), 1);
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText projectName = (EditText) findViewById(R.id.projectName);
                Project project;

                if(device != null) {
                    project = new Project(projectName.getText().toString(), device);
                } else {
                    project = new Project(projectName.getText().toString(), null);
                }

                project.save();

                Intent intent = new Intent();
                intent.putExtra("project", project);


                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, intent);
                } else {
                    getParent().setResult(Activity.RESULT_OK, intent);
                }

                Toast.makeText(getBaseContext(), "Created " + project.getName(), Toast.LENGTH_SHORT).show();

                finish();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if (resultCode == RESULT_OK){
                this.device = (BluetoothDevice) data.getSerializableExtra("btdevice");
            }
        }
    }
}
