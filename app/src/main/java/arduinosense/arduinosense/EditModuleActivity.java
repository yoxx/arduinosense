package arduinosense.arduinosense;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;

import arduinosense.arduinosense.models.ButtonModule;
import arduinosense.arduinosense.models.ConsoleModule;
import arduinosense.arduinosense.models.Module;
import arduinosense.arduinosense.models.SliderModule;
import arduinosense.arduinosense.utils.Utils;


public class EditModuleActivity extends AppCompatActivity {
    private Context context;
    private RelativeLayout layout;
    private RadioButton radio;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_module);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.context = getBaseContext();
        this.layout = (RelativeLayout) findViewById(R.id.relative_layout_module);

        final Module tmp = (Module) getIntent().getSerializableExtra("module");
        setTitle("Module: " + tmp.getName());

        final EditText module_name = (EditText) findViewById(R.id.module_name);
        final EditText module_adres = (EditText) findViewById(R.id.module_address);

        module_name.setText(tmp.getName());
        module_adres.setText("" + tmp.getAddress());

        radioGroup = (RadioGroup) findViewById(R.id.radiogroup_module);

        if (tmp.getIO()){
            radio = (RadioButton) findViewById(R.id.module_output);
            radio.toggle();
        } else if (!tmp.getIO()){
            radio = (RadioButton) findViewById(R.id.module_input);
            radio.toggle();
        }


        Button save = (Button) findViewById(R.id.module_save);
        LayoutInflater inflater;
        RelativeLayout module_layout;
        RelativeLayout.LayoutParams params;

        switch (tmp.getClazz()){
            case "arduinosense.arduinosense.models.Module":
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch(checkedId){
                            case R.id.module_input:
                                tmp.setIO(Utils.INPUT);
                                break;
                            case R.id.module_output:
                                tmp.setIO(Utils.OUTPUT);
                                break;
                        }
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!tmp.getName().equals(module_adres.getText().toString())){
                            tmp.setName(module_name.getText().toString());
                        }

                        if (tmp.getAddress() != Integer.parseInt(module_adres.getText().toString())){
                            tmp.setAddress(Integer.parseInt(module_adres.getText().toString()));
                        }

                        tmp.save();

                        Intent intent = new Intent();
                        intent.putExtra("module", tmp);
                        if (getParent() == null) {
                            setResult(4, intent);
                        } else {
                            getParent().setResult(4, intent);
                        }

                        finish();
                    }
                });
                break;
            case "arduinosense.arduinosense.models.ButtonModule":
                // Retrieve the buttonModule
                final ButtonModule btn = (ButtonModule) tmp;
                // get the XML inflator
                inflater = LayoutInflater.from(context);
                // Retrieve the layout
                module_layout = (RelativeLayout) inflater.inflate(R.layout.switch_module, null, false);
                // Set the layout parameters
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                // Add rules to the parameters
                params.addRule(RelativeLayout.BELOW, R.id.radiogroup_module);
                // Add the parameters to the layout
                module_layout.setLayoutParams(params);
                // Add the layout to our root layout
                layout.addView(module_layout);

                if (findViewById(R.id.switch1) != null){
                    Switch sw = (Switch)findViewById(R.id.switch1);
                    sw.setChecked(btn.isToggle());
                    sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            btn.setToggle(isChecked);
                        }
                    });
                }

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch(checkedId){
                            case R.id.module_input:
                                btn.setIO(Utils.INPUT);
                                break;
                            case R.id.module_output:
                                btn.setIO(Utils.OUTPUT);
                                break;
                        }
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!btn.getName().equals(module_adres.getText().toString())){
                            btn.setName(module_name.getText().toString());
                        }

                        if (btn.getAddress() != Integer.parseInt(module_adres.getText().toString())){
                            btn.setAddress(Integer.parseInt(module_adres.getText().toString()));
                        }

                        btn.save();

                        Intent intent = new Intent();
                        intent.putExtra("button", btn);
                        if (getParent() == null) {
                            setResult(1, intent);
                        } else {
                            getParent().setResult(1, intent);
                        }

                        finish();
                    }
                });
                break;
            case "arduinosense.arduinosense.models.SliderModule":
                final SliderModule slider = (SliderModule) tmp;

                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.radiogroup_module);
                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params2.addRule(RelativeLayout.ABOVE, R.id.module_save);
                params2.setMargins(0, 0, 0, 135);

                int id1 = 5;
                int id2 = 10;
                final EditText start_range = Utils.addEditTextToView(context, layout, "Start Range", params, id1);
                final EditText end_range = Utils.addEditTextToView(context, layout, "End Range", params2, id2);

                start_range.setText("" + slider.getMinRange());
                end_range.setText("" + slider.getMaxRange());

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.module_input:
                                slider.setIO(Utils.INPUT);
                                break;
                            case R.id.module_output:
                                slider.setIO(Utils.OUTPUT);
                                break;
                        }
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!slider.getName().equals(module_adres.getText().toString())){
                            slider.setName(module_name.getText().toString());
                        }

                        if (slider.getAddress() != Integer.parseInt(module_adres.getText().toString())){
                            slider.setAddress(Integer.parseInt(module_adres.getText().toString()));
                        }

                        if(slider.getMinRange() != Integer.parseInt(start_range.getText().toString())){
                            slider.setMinRange(Integer.parseInt(start_range.getText().toString()));
                        }

                        if(slider.getMaxRange() != Integer.parseInt(end_range.getText().toString())){
                            slider.setMaxRange(Integer.parseInt(end_range.getText().toString()));
                        }

                        slider.save();

                        Intent intent = new Intent();
                        intent.putExtra("slider", slider);
                        if (getParent() == null) {
                            setResult(2, intent);
                        } else {
                            getParent().setResult(2, intent);
                        }

                        finish();
                    }
                });
                break;
            case "arduinosense.arduinosense.models.ConsoleModule":
                final ConsoleModule console = (ConsoleModule) tmp;
                // get the XML inflator
                inflater = LayoutInflater.from(context);
                // Retrieve the layout
                module_layout = (RelativeLayout) inflater.inflate(R.layout.switch_module, null, false);
                // Set the layout parameters
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                // Add rules to the parameters
                params.addRule(RelativeLayout.BELOW, R.id.radiogroup_module);
                // Add the parameters to the layout
                module_layout.setLayoutParams(params);
                // Add the layout to our root layout
                layout.addView(module_layout);
                Switch sw = (Switch) findViewById(R.id.switch1);
                sw.setChecked(console.getShowDetail());
                sw.setText("Show detailed Logging");
                sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        console.setShowDetail(isChecked);
                    }
                });

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch(checkedId){
                            case R.id.module_input:
                                console.setIO(Utils.INPUT);
                                break;
                            case R.id.module_output:
                                console.setIO(Utils.OUTPUT);
                                break;
                        }
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!console.getName().equals(module_adres.getText().toString())) {
                            console.setName(module_name.getText().toString());
                        }

                        if (console.getAddress() != Integer.parseInt(module_adres.getText().toString())) {
                            console.setAddress(Integer.parseInt(module_adres.getText().toString()));
                        }

                        console.save();

                        Intent intent = new Intent();
                        intent.putExtra("console", console);
                        if (getParent() == null) {
                            setResult(3, intent);
                        } else {
                            getParent().setResult(3, intent);
                        }

                        finish();
                    }
                });
                break;
        }

        module_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setTitle("Module: " + module_name.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
