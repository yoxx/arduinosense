package arduinosense.arduinosense;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

import arduinosense.arduinosense.bluetooth.BluetoothController;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.models.Project;
import arduinosense.arduinosense.utils.ProjectAdapter;

public class HomeScreen extends AppCompatActivity {
    private ArrayList<Project> projects;
    private ProjectAdapter adapter;
    private ListView projectList;
    private boolean empty = false;
    private TextView no_project;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        // This instantiates the connection to the database when the app first starts.
        DatabaseHelper.getHelper(this);
        // This instantiates the bluetooth controller when the app first starts
        BluetoothController.getHelper(this);

        // Load any current projects in the database and add this to the list
        this.projects = Project.where();

        if (this.projects.isEmpty()) {
            this.no_project = (TextView) findViewById(R.id.no_project);
            this.no_project.setText("No projects");
            this.empty = true;
        }

        // Setup the listview where we can show our projects
        this.projectList= (ListView) findViewById(R.id.projectList);
        this.adapter = new ProjectAdapter(this, android.R.layout.simple_list_item_1, projects);
        this.projectList.setAdapter(adapter);

        this.projectList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Project project = (Project) projects.get(position);
                Intent intent = new Intent(HomeScreen.this, ProjectActivity.class);
                intent.putExtra("project", project);
                startActivityForResult(intent,1);
            }
        });


        FloatingActionButton createButton = (FloatingActionButton) findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(HomeScreen.this, CreateActivity.class), 2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1){
            if (resultCode == RESULT_OK){
                Project project = (Project) data.getSerializableExtra("project");
                for(int count = 0; count < this.projects.size(); count ++){
                    if (this.projects.get(count).getId() == project.getId()){
                        this.projects.get(count).setName(project.getName());
                        this.projects.get(count).setBTController(project.getBTController());
                        this.projects.get(count).setModules(project.getModules());
                    }
                    this.adapter.notifyDataSetChanged();
                }
            }
            if (resultCode == 5){
                Project project = (Project) data.getSerializableExtra("project");
                for(Project pr : this.projects){
                    if (pr.getId() == project.getId()){
                        this.projects.remove(pr);
                        this.adapter.notifyDataSetChanged();
                        if (this.projects.isEmpty()) {
                            this.no_project = (TextView) findViewById(R.id.no_project);
                            this.no_project.setText("No projects");
                            this.empty = true;
                        }
                    }
                }
            }
        }
        if(requestCode == 2){
            if (resultCode == RESULT_OK){
                // Get the id of the just created project
                Project project = (Project) data.getSerializableExtra("project");

                // Check if the list was empty when the app started if so clear it.
                if (this.empty){
                    this.no_project.setText("");
                    this.empty = false;
                }

                // Add the newly created project to the list
                this.projects.add(project);

                // Notify the view the list changed
                this.adapter.notifyDataSetChanged();
            }
        }
    }
}


