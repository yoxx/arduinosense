package arduinosense.arduinosense;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import arduinosense.arduinosense.models.ButtonModule;
import arduinosense.arduinosense.models.ConsoleModule;
import arduinosense.arduinosense.models.Module;
import arduinosense.arduinosense.models.Project;
import arduinosense.arduinosense.models.SliderModule;

public class ModulesSettingsActivity extends AppCompatActivity {

    private Project  project;
    private ArrayAdapter<Module> adapter;
    private ListView moduleList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modules_edit);

        this.project = (Project) getIntent().getSerializableExtra("project");
        this.moduleList= (ListView) findViewById(R.id.moduleList);
        this.adapter = new ArrayAdapter<Module>(this, android.R.layout.simple_list_item_1, project.getModules());
        this.moduleList.setAdapter(adapter);



        this.moduleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Module mod  = (Module) project.getModules().get(position);
                Intent intent = new Intent(ModulesSettingsActivity.this, EditModuleActivity.class);
                intent.putExtra("module", mod);
                startActivityForResult(intent,1);
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == 1) {
                ButtonModule btn = (ButtonModule) data.getSerializableExtra("button");

                for (int count = 0; count < this.project.getModules().size(); count++) {
                    Module tmp = this.project.getModules().get(count);

                    if (tmp.getClazz().equals(btn.getClazz()) && tmp.getId() == btn.getId()) {
                        ButtonModule listBtn = (ButtonModule) this.project.getModules().get(count);
                        listBtn.setName(btn.getName());
                        listBtn.setAddress(btn.getAddress());
                        listBtn.setIO(btn.getIO());

                        listBtn.setToggle(btn.isToggle());
                        listBtn.setState(btn.getState());
                    }
                    this.adapter.notifyDataSetChanged();
                }
            }
            if (resultCode == 2) {
                SliderModule slider = (SliderModule) data.getSerializableExtra("slider");

                for (int count = 0; count < this.project.getModules().size(); count++) {
                    Module tmp = this.project.getModules().get(count);
                    if (tmp.getClazz().equals(slider.getClazz()) && tmp.getId() == slider.getId()) {
                        SliderModule listSlider = (SliderModule) this.project.getModules().get(count);
                        listSlider.setName(slider.getName());
                        listSlider.setAddress(slider.getAddress());
                        listSlider.setIO(slider.getIO());

                        listSlider.setMaxRange(slider.getMaxRange());
                        listSlider.setMinRange(slider.getMinRange());
                    }
                    this.adapter.notifyDataSetChanged();
                }
            }
            if (resultCode == 3) {
                ConsoleModule console = (ConsoleModule) data.getSerializableExtra("console");

                for (int count = 0; count < this.project.getModules().size(); count++) {
                    Module tmp = this.project.getModules().get(count);

                    if (tmp.getClazz().equals(console.getClazz()) && tmp.getId() == console.getId()) {
                        ConsoleModule listConsole = (ConsoleModule) this.project.getModules().get(count);
                        listConsole.setName(console.getName());
                        listConsole.setAddress(console.getAddress());
                        listConsole.setIO(console.getIO());

                        listConsole.setShowDetail(console.getShowDetail());
                    }
                    this.adapter.notifyDataSetChanged();
                }
            }
            if (resultCode == 4) {
                Module module = (Module) data.getSerializableExtra("module");

                for (int count = 0; count < this.project.getModules().size(); count++) {
                    Module tmp = this.project.getModules().get(count);

                    if (tmp.getClazz().equals(module.getClazz()) && tmp.getId() == module.getId()) {
                        Module listModule = (Module) this.project.getModules().get(count);
                        listModule.setName(module.getName());
                        listModule.setAddress(module.getAddress());
                        listModule.setIO(module.getIO());
                    }
                    this.adapter.notifyDataSetChanged();
                }

            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("project", project);
        if (getParent() == null) {
            setResult(2, intent);
        } else {
            getParent().setResult(2, intent);
        }
        finish();
    }
}
