package arduinosense.arduinosense;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.models.ButtonModule;
import arduinosense.arduinosense.models.ConsoleModule;
import arduinosense.arduinosense.models.Module;
import arduinosense.arduinosense.models.Project;
import arduinosense.arduinosense.models.SliderModule;

public class ProjectActivity extends AppCompatActivity {
    private Project project;
    private RelativeLayout layout;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_project);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get the base context for further usage
        this.context = getBaseContext();
        // Retrieve the id from the previous actvity and find the project
        this.project = (Project) getIntent().getSerializableExtra("project");
        this.project.setMoveable(false);
        for (Module mod : this.project.getModules()){
            mod.setMoveable(this.project.getMoveable());
        }

        if (this.project.getBTController() != null) {
            // Check if bluetooth is enabled
            if(this.project.getBTController().checkBluetooth(this)) {
                this.project.getBTController().connectDevice(this.project.getModules(), this);
            }
        } else {
            Toast.makeText(this.context, "There is no bluetooth device configured for this project", Toast.LENGTH_SHORT).show();
        }

        // Set the title of the Activity
        setTitle("Project: " + this.project.getName());


        // Get the current Layout
        this.layout = (RelativeLayout) findViewById(R.id.relative_layout);
        // Go trough the modules list and add them to the layout
        for(int i = 0; i < project.getModules().size(); i++){
            switch(project.getModules().get(i).getClazz()){
                case "arduinosense.arduinosense.models.Module":
                    Module mod = (Module) project.getModules().get(i);
                    mod.addToView(getBaseContext(),layout, this.project.getBTController());
                    if (this.project.getBTController() != null && mod.getIO() && this.project.getBTController().isConnected()) {
                        this.project.getBTController().startDataListener(this.project.getModules(), this);
                    }
                    break;
                case "arduinosense.arduinosense.models.ButtonModule":
                    ButtonModule btn = (ButtonModule) project.getModules().get(i);
                    btn.addToView(getBaseContext(),layout, this.project.getBTController());
                    break;
                case "arduinosense.arduinosense.models.SliderModule":
                    SliderModule sld = (SliderModule) project.getModules().get(i);
                    sld.addToView(getBaseContext(),layout, this.project.getBTController());
                    if (this.project.getBTController() != null && this.project.getBTController().isConnected()) {
                        this.project.getBTController().startDataListener(this.project.getModules(), this);
                    }
                    break;
                case "arduinosense.arduinosense.models.ConsoleModule":
                    ConsoleModule cons = (ConsoleModule) project.getModules().get(i);
                    if (this.project.getBTController() != null && this.project.getBTController().isConnected()) {
                                this.project.getBTController().startDataListener(this.project.getModules(), this);
                        }
                    cons.addToView(getBaseContext(),layout, this.project.getBTController());
                    break;
            }
        }

        FloatingActionButton addModuleButton = (FloatingActionButton) findViewById(R.id.addModuleButton);
        addModuleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProjectActivity.this, AddModuleActivity.class);
                intent.putExtra("project", project); //Pass whole project as class
                startActivityForResult(intent,1);
            }
        });

        //Retrieve the whole project as sequence of bytes
        this.project = (Project) getIntent().getSerializableExtra("project");

        // Set the title of the Activity
        setTitle("Project: " + this.project.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    //Link menu items to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.putExtra("project", this.project); //Pass whole project as class
                this.startActivityForResult(intent,2);

                break;
            case R.id.menu_movable:
                if(!item.isChecked()){
                    this.project.setMoveable(true);
                    for(Module mod : this.project.getModules()){
                        mod.setMoveable(true);
                    }
                    item.setChecked(this.project.getMoveable());
                } else {
                    this.project.setMoveable(false);
                    for(Module mod : this.project.getModules()){
                        mod.setMoveable(false);
                    }
                    item.setChecked(this.project.getMoveable());
                }
                break;
            case R.id.menu_edit:
                Intent intent2 = new Intent(this, ModulesSettingsActivity.class);
                intent2.putExtra("project", this.project); //Pass whole project as class
                this.startActivityForResult(intent2, 2);
                break;
            case R.id.menu_reconnect:
                if (this.project.getBTController() != null) {
                    // Check if bluetooth is enabled
                    if(this.project.getBTController().checkBluetooth(this)) {
                        this.project.getBTController().connectDevice(this.project.getModules(), this);
                        if (this.project.getBTController().isConnected()) {
                            this.project.getBTController().startDataListener(this.project.getModules(), this);
                        }
                    }
                } else {
                    Toast.makeText(this.context, "There is no bluetooth device configured for this project", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.menu_remove:
                new AlertDialog.Builder(this)
                        .setTitle("Delete " + project.getName())
                        .setMessage("Do you really want to delete this project?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                project.delete();
                                Intent intent = new Intent();
                                intent.putExtra("project", project);
                                if (getParent() == null) {
                                    setResult(5, intent);
                                } else {
                                    getParent().setResult(5, intent);
                                }
                                finish();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if (resultCode == 1){
                this.project = (Project) data.getSerializableExtra("project");
                ButtonModule bt_module = (ButtonModule) data.getSerializableExtra("button");
                bt_module.setMoveable(this.project.getMoveable());
                bt_module.addToView(this.getApplicationContext(), this.layout, this.project.getBTController());
            }
            if (resultCode == 2){
                this.project = (Project) data.getSerializableExtra("project");
                SliderModule sliderModule = (SliderModule) data.getSerializableExtra("slider");
                sliderModule.setMoveable(this.project.getMoveable());
                sliderModule.addToView(this.getApplicationContext(), this.layout, this.project.getBTController());
            }
            if (resultCode == 3){
                this.project = (Project) data.getSerializableExtra("project");
                ConsoleModule consoleModule = (ConsoleModule) data.getSerializableExtra("console");
                consoleModule.setMoveable(this.project.getMoveable());
                consoleModule.addToView(this.getApplicationContext(), this.layout, this.project.getBTController());
            }
            if (resultCode == 4){
                this.project = (Project) data.getSerializableExtra("project");
                Module module = (Module) data.getSerializableExtra("module");
                module.setMoveable(this.project.getMoveable());
                module.addToView(this.getApplicationContext(), this.layout, this.project.getBTController());
            }
        }
        if (requestCode == 2){
            if (resultCode == 1){
                BluetoothDevice bt = this.project.getBTController();
                this.project = (Project) data.getSerializableExtra("project");
                setTitle(this.project.getName());
                    if (this.project.getBTController() != null){
                        if (bt == null || bt.getId() != this.project.getBTController().getId()){
                        this.project.getBTController().connectDevice(this.project.getModules(), this);
                    }
                }
            }
            if (resultCode == 2){
                this.project = (Project) data.getSerializableExtra("project");
                // Get the current Layout
                this.layout.removeAllViews();
                // Go trough the modules list and add them to the layout
                for(int i = 0; i < project.getModules().size(); i++){
                    switch(project.getModules().get(i).getClazz()){
                        case "arduinosense.arduinosense.models.Module":
                            Module mod = (Module) project.getModules().get(i);
                            mod.addToView(getBaseContext(),layout, this.project.getBTController());
                            mod.setMoveable(this.project.getMoveable());
                            if (this.project.getBTController() != null && mod.getIO()) {
                                this.project.getBTController().startDataListener(this.project.getModules(), this);
                            }
                            break;
                        case "arduinosense.arduinosense.models.ButtonModule":
                            ButtonModule btn = (ButtonModule) project.getModules().get(i);
                            if (this.project.getBTController() != null && btn.getIO()) {
                                this.project.getBTController().startDataListener(this.project.getModules(), this);
                            }
                            btn.addToView(getBaseContext(),layout, this.project.getBTController());
                            btn.setMoveable(this.project.getMoveable());
                            break;
                        case "arduinosense.arduinosense.models.SliderModule":
                            SliderModule sld = (SliderModule) project.getModules().get(i);
                            if (this.project.getBTController() != null && sld.getIO()) {
                                this.project.getBTController().startDataListener(this.project.getModules(), this);
                            }
                            sld.addToView(getBaseContext(),layout, this.project.getBTController());
                            sld.setMoveable(this.project.getMoveable());
                            break;
                        case "arduinosense.arduinosense.models.ConsoleModule":
                            ConsoleModule cons = (ConsoleModule) project.getModules().get(i);
                            if (this.project.getBTController() != null && this.project.getBTController().isConnected()) {
                                this.project.getBTController().startDataListener(this.project.getModules(), this);
                            }
                            cons.setMoveable(this.project.getMoveable());
                            cons.addToView(getBaseContext(),layout, this.project.getBTController());
                            break;
                    }
                }
            }
        }
        if (requestCode == 10){
            if (!this.project.getBTController().isConnected()) {
                this.project.getBTController().connectDevice(this.project.getModules(), this);
            }
            this.project.getBTController().startDataListener(this.project.getModules(), this);

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("project", project);
        if (getParent() == null) {
            setResult(RESULT_OK, intent);
        } else {
            getParent().setResult(RESULT_OK, intent);
        }
        if (this.project.getBTController() != null) {
            this.project.getBTController().stopDataListener(this);
            this.project.getBTController().closeConnection();
        }
        finish();
    }
}
