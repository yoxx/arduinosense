package arduinosense.arduinosense;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.models.Project;

public class SettingsActivity extends AppCompatActivity {
    private Project project;
    private BluetoothDevice btdevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Button bt_button = (Button) findViewById(R.id.linkDeviceButton);
        Button save_button = (Button) findViewById(R.id.saveSettingsButton);

        bt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SettingsActivity.this, BluetoothSetup.class), 1);
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText projectNameField = (EditText) findViewById(R.id.projectNameField);
                project = (Project) getIntent().getSerializableExtra("project");

                if (project.getName() != projectNameField.getText().toString()) {
                    project.setName(projectNameField.getText().toString());
                }

                Intent intent = new Intent();
                intent.putExtra("project", project);
                if (getParent() == null) {
                    setResult(1, intent);
                } else {
                    getParent().setResult(1, intent);
                }

                project.setBTController(btdevice);

                project.save();


                Toast.makeText(getBaseContext(), "Saved " + project.getName(), Toast.LENGTH_SHORT).show();


                finish();


            }


        });





        //Retrieve the whole project as sequence of bytes
        this.project = (Project) getIntent().getSerializableExtra("project");

        // Set the title of the Activity
        setTitle("Settings for: " + this.project.getName());


        //Sets the content of the textfield to the name of the current project
        final EditText text = (EditText)findViewById(R.id.projectNameField);
        text.setText(this.project.getName());
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setTitle("Settings for: " + text.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    //Link menu itenms to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menu_edit:
                Intent intent2 = new Intent(this, AddModuleActivity.class);
                this.startActivity(intent2);
                // another startActivity, this is for item with id "menu_item2"
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if (resultCode == RESULT_OK){
                this.btdevice = (BluetoothDevice) data.getSerializableExtra("btdevice");
            }
        }


    }



}
