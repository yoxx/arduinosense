package arduinosense.arduinosense.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by Yoxx on 09/04/16.
 */
public class BluetoothController {
    /* Our globals */
    private static BluetoothController mInstance;
    private static final UUID MY_UUID = UUID.randomUUID();
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private Activity activity;
    private boolean connected = false;

    /**
     * Constructor of out BluetoothController class
     *
     * @param ctx Activity
     */
    public BluetoothController(Activity ctx) {
        this.activity = ctx;
    }

    /**
     * Singelton instancetiator
     * @param ctx Activity
     * @return BluetoothController
     */
    public static synchronized BluetoothController getHelper (Activity ctx){
        if (mInstance == null){
            mInstance = new BluetoothController(ctx);
        }
        return mInstance;
    }

    /**
     * returns the instance of the bluetooth controller without checking if it exists
     *
     * @return BluetoothController
     */
    public static synchronized BluetoothController getInstance(){
        return mInstance;
    }

    /**
     * Check if device has Bluetooth turned on
     *
     * @param act the current activity
     * @return boolean
     */
    public boolean checkBluetooth(Activity act)
    {
        BluetoothAdapter mBtAdapter= BluetoothAdapter.getDefaultAdapter();
        if(mBtAdapter==null) {
            Toast.makeText(activity, "Device does not support Bluetooth", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!mBtAdapter.isEnabled()) {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                act.startActivityForResult(enableBtIntent, 10);
                return false;
            }
            return true;
        }
    }

    /**
     * Checks if we are connected to a device
     * @return boolean
     */
    public boolean isConnected(){
        return this.connected;
    }

    /**
     * Connects to device and tries if sending data is possible
     *
     * @param mac_address the mac-address of the device you want to connect with
     * @return boolean
     */
    public boolean connectDevice(String mac_address) {
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        // Set up a pointer to the remote device using its mac-address.
        BluetoothDevice device = btAdapter.getRemoteDevice(mac_address);
        if (device != null) {
            //Attempt to create a bluetooth socket for communication
            try {
                btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e1) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "ERROR - Could not create Bluetooth socket", Toast.LENGTH_SHORT).show();
                    }
                });
                return false;
            }

            // Establish the connection.
            try {
                btSocket.connect();
            } catch (IOException e) {
                try {
                    Log.e("",e.getMessage());
                    try {
                        // You need to have a fallback because android 4.2.1 and up has issues with the straightforward method
                        Log.e("","trying fallback...");
                        btSocket =(BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
                        btSocket.connect();
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(activity, "Connected...", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.e("","Connected");
                    }
                    catch (Exception e2) {
                        Log.e("", "Couldn't establish Bluetooth connection!");
                        btSocket.close();        //If IO exception occurs attempt to close socket
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(activity, "ERROR - Could not connect to device", Toast.LENGTH_SHORT).show();
                            }
                        });
                        return false;
                    }
                } catch (IOException e2) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, "ERROR - Could connect to device and could not close Bluetooth socket", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return false;
                }
            }

            // Send bogus data to check the connection
            this.connected = sendData(0, 1);
            return this.connected;
        } else {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "ERROR - Device not in reach?", Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
    }

    /**
     * Send data to the device
     *
     * @param address the address we want to send stuff too
     * @param value the value we want to send
     * @return
     */
    public boolean sendData(int address, int value){
        // throw our address and value in a byte array
        byte[] msgBuffer = new byte[] {(byte) address, (byte) value};
        // Create a data stream so we can talk to the device
        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "ERROR - Could not open stream are you connected?", Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
        try {
            //attempt to place data on the outstream to the BT device
            outStream.write(msgBuffer);
        } catch (IOException e) {
            //if the sending fails this is most likely because device is no longer there
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "ERROR - Could not send data", Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
        return true;
    }

    /**
     * Sends a string of data as command
     *
     * @param command String
     * @return boolean
     */
    public boolean sendStringData(String command) {
        // throw our address and value in a byte array
        byte[] msgBuffer =  command.getBytes();
        // Create a data stream so we can talk to the device
        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "ERROR - Could not open stream are you connected?", Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
        try {
            //attempt to place data on the outstream to the BT device
            outStream.write(msgBuffer);
        } catch (IOException e) {
            //if the sending fails this is most likely because device is no longer there
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "ERROR - Could not send data", Toast.LENGTH_SHORT).show();
                }
            });
            return false;
        }
        return true;
    }

    /**
     * Recieve data from bluetooth in bytes
     *
     * @return int[]
     */
    public int[] recieveData() throws IOException {
        byte[] bytes = new byte[2];
        int iterator = 0;
            try {
                InputStream input = btSocket.getInputStream();
                int bytesAvailable = input.available();
                if (bytesAvailable > 0) {
                    byte[] curBuf = new byte[bytesAvailable];
                    input.read(curBuf);
                    for (byte b : curBuf) {
                        if (iterator == 1) {
                            bytes[iterator] = b;
                            return new int[]{(int) bytes[0], (int) bytes[1]};
                        } else {
                            bytes[iterator] = b;
                        }
                        iterator++;
                    }
                }
                return new int[]{ 0, 0};
        } catch (IOException ex) {
            ex.printStackTrace();
            return new int[]{0, 0};
        }
    }

    /**
     * Close the current socket and set our connection status to false
     * @return boolean
     */
    public boolean closeConnection(){
        try {
            if(btSocket != null) {
                this.btSocket.close();
            }
            this.connected = false;
            return true;
        } catch (IOException ex){
            ex.printStackTrace();
            return false;
        }
    }
}
