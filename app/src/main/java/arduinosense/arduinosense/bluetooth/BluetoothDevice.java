package arduinosense.arduinosense.bluetooth;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.models.Module;

/**
 * Created by Yoxx on 24/03/16.
 */
public class BluetoothDevice implements Serializable {
    private int id;
    private String deviceName;
    private String deviceMac;

    /**
     * Constructor for the bluetooth
     *
     * @param deviceName String
     * @param deviceMac  String
     */
    public BluetoothDevice(String deviceName, String deviceMac) {
        this.deviceName = deviceName;
        this.deviceMac = deviceMac;
    }

    /**
     * Returns the ID of an object
     *
     * @return int
     */
    public int getId() {
        return this.id;
    }

    /**
     * Sets the id of the module
     *
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the name of the bluetooth device
     * (Can return null when the device does not respond on the name call)
     *
     * @return String
     */
    public String getDeviceName() {
        return this.deviceName;
    }

    /**
     * Returns the mac of the bluetooth device
     * (A bluetooth device will always give his mac but not always his name)
     *
     * @return String
     */
    public String getDeviceMac() {
        return this.deviceMac;
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.BTControllerTable.NAME, this.deviceName);
        values.put(DatabaseInfo.BTControllerTable.MAC, this.deviceMac);

        if (this.id == 0) {
            int id = db.insert(DatabaseInfo.BTControllerTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            db.update(DatabaseInfo.BTControllerTable.TABLE, values, "_id = " + this.id);
        }
    }

    /**
     * Sets the device name
     *
     * @param name String
     */
    public void setDeviceName(String name) {
        this.deviceName = name;
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        Cursor cursor = db.query(DatabaseInfo.BTControllerTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        // Move to the next item and read the controller from the database
        while (cursor.moveToNext()) {
            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            String device_mac = (String) cursor.getString(cursor.getColumnIndex("mac"));

            BluetoothDevice controller = new BluetoothDevice(name, device_mac);
            controller.setId(id);

            list.add(controller);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return BluetoothDevice
     */
    static public BluetoothDevice find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.BTControllerTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        String device_mac = (String) cursor.getString(cursor.getColumnIndex("mac"));

        BluetoothDevice controller = new BluetoothDevice(name, device_mac);
        controller.setId(id);
        cursor.close();

        return controller;
    }

    /**
     * Checks if we are currently connected to bluetooth
     *
     * @param act Activity
     * @return boolean
     */
    public boolean checkBluetooth(Activity act) {
        BluetoothController btController = BluetoothController.getInstance();
        return btController.checkBluetooth(act);
    }

    /**
     * Connect to the saved device
     */
    public void connectDevice(ArrayList<Module> list, Activity activity) {
        new ConnectDevice(this.deviceMac,list,activity);
    }

    /**
     * Returns our connection status
     * @return boolean
     */
    public boolean isConnected(){
        BluetoothController btController = BluetoothController.getInstance();
        return btController.isConnected();
    }

    /**
     * Close the bluetooth connection
     */
    public void closeConnection() {
        BluetoothController btController = BluetoothController.getInstance();
        btController.closeConnection();
    }

    /**
     * Start listening for incoming data
     *
     * @param modules ArrayList
     */
    public void startDataListener(ArrayList<Module> modules, Activity act) {
        RecieveData rc = RecieveData.getInstance(this.deviceName, act);
        rc.setModuleList(modules);
    }

    /**
     * Stop listening the incoming connection
     */
    public void stopDataListener(Activity act) {
        RecieveData rc = RecieveData.getInstance(this.deviceName, act);
        rc.setStop(true);
    }

    /**
     * Send two bytes of data
     *
     * @param name    String
     * @param address int
     * @param value   int
     */
    public void sendData(String name, int address, int value) {
        new SendData(name, address, value, "");
    }

    /**
     * Send a string command to the arduino
     *
     * @param name       the name of the module sending the command
     * @param tmpCommand String: A # will get added to the command before this is send.
     */
    public void sendStringData(String name, String tmpCommand) {
        String command = "#" + tmpCommand;
        new SendData(name, command, "string");
    }

    /**
     * Send data for a time
     *
     * @param name    String
     * @param address int
     * @param value   int
     */
    public void sendLoopData(String name, int address, int value) {
        SendData.setInstance(name, address, value);
    }

    /**
     * Stop the sendData loop
     */
    public void stopSendLoopData() {
        SendData sd = SendData.getInstance();
        sd.setStop(true);
    }
}

/**
 * Class needed to recieve data in a seperate thread
 */
class RecieveData implements Runnable {
    /* Private variables */
    private ArrayList<Module> moduleList;
    private volatile boolean stop = false;
    private static RecieveData mInstance;
    private Activity activity;
    private int prevValue;

    /**
     * Constructor starts the thread
     *
     * @param name String: Name of the device we are recieving from
     */
    public RecieveData(String name, Activity activity) {
        this.activity = activity;
        Thread t = new Thread(this, "DataListener_" + name);
        t.start();
    }

    /**
     * Sets an instance of data recievers
     *
     * @param name String
     * @return RecieveData
     */
    public static synchronized RecieveData getInstance(String name, Activity activity) {
        if (mInstance == null) {
            mInstance = new RecieveData(name, activity);
        }
        return mInstance;
    }

    /**
     * Stop recieving data
     * @param stop
     */
    public void setStop(Boolean stop) {
        this.stop = stop;
        this.mInstance = null;
    }

    /**
     * Set the modulelist for looping pas the modules and checking their channels
     * @param modules ArrayList
     */
    public void setModuleList(ArrayList<Module> modules) {
        this.moduleList = modules;
    }

    @Override
    public void run() {
        // Get a bluetooth instance
        BluetoothController bt = BluetoothController.getInstance();
        // Loop recieving data
        while (!stop) {
            int[] i = null;
            try {
                i = bt.recieveData();
            } catch(IOException ex){
                ex.printStackTrace();
                stop = true;
            }
            if (i != null) {
                // if we already have this value dont bother sending it to the main thread
                if (i[0] > 0 && prevValue != i[1]) {
                    prevValue = i[1];
                    Log.e("", "Channel: " + i[0] + " value: " + i[1]);
                    // Check what modules should recieve this data and send it to them
                    for (final Module module : this.moduleList) {
                        if (module.getClazz().equals("arduinosense.arduinosense.models.ConsoleModule")
                                || (module.getAddress() == i[0] && !module.getIO())) {
                            final int address = i[0];
                            final int value = i[1];
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    module.showValueInView(address, value);
                                }
                            });

                            try {
                                synchronized (this) {
                                    this.wait(1000);
                                }
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                Log.e("", "Empty list");
            }
            try {
                synchronized (this) {
                    this.wait(500);
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}

/**
 * Class for sending data
 */
class SendData implements Runnable {
    /* Our variables */
    private int address;
    private int value;
    private String action;
    private String command;
    private volatile boolean stop = false;
    private static SendData instance;

    /**
     * Send data using a seperate thread
     * @param name String
     * @param address int
     * @param value int
     * @param action String
     */
    public SendData(String name, int address, int value, String action) {
        this.address = address;
        this.value = value;
        this.action = action;
        this.stop = false;

        Thread t = new Thread(this, "DataListener_" + name);
        t.start();
    }

    /**
     * Send data using an action
     * @param name String
     * @param command String
     * @param action String
     */
    public SendData(String name, String command, String action) {
        this.command = command;
        this.action = action;
        this.stop = false;

        Thread t = new Thread(this, "DataListener_" + name);
        t.start();
    }

    /**
     * Make a singelton instance for sending data multiple times
     * @param name String
     * @param address int
     * @param value int
     * @return SendData
     */
    public static synchronized SendData setInstance(String name, int address, int value) {
        if (instance == null) {
            instance = new SendData(name, address, value, "loop");
        }
        return instance;
    }

    public static synchronized SendData getInstance() {
        return instance;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
        instance.
        instance = null;
    }

    @Override
    public void run() {
        BluetoothController bt = BluetoothController.getInstance();
        if (action.equals("loop")) {
            while (!stop) {
                bt.sendData(this.address, this.value);
                Log.e("", "Data send: [" + this.address + ", " + this.value);
                try {
                    synchronized (this) {
                        this.wait(100);
                    }
                } catch (InterruptedException ex) {
                    stop = true;
                }
            }
        }
        if (action.equals("string")) {
            bt.sendStringData(this.command);
        } else {
            bt.sendData(this.address, this.value);
            try {
                synchronized (this) {
                    this.wait(100);
                }
            } catch (InterruptedException ex) {
                stop = true;
            }
        }
    }
}

class ConnectDevice implements Runnable {
    private ArrayList<Module> list;
    private Activity activity;
    private String device_mac;

    public ConnectDevice(String mac, ArrayList<Module> list, Activity activity) {
        this.device_mac = mac;
        this.activity = activity;
        this.list = list;

        Thread t = new Thread(this, "connect_" + mac);
        t.start();
    }

    @Override
    public void run() {
        BluetoothController btController = BluetoothController.getInstance();
        boolean connected = btController.connectDevice(this.device_mac);
        if (connected) {
            for (int count = 0; count < this.list.size(); count++) {
                if (!this.list.get(count).getIO()) {
                    RecieveData rc = RecieveData.getInstance(this.device_mac, this.activity);
                    rc.setModuleList(this.list);
                }
            }
        }
    }
}
