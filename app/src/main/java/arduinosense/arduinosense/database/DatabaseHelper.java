package arduinosense.arduinosense.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Yoxx on 29/03/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;			// SINGLETON TRUC
    public static final String dbName = "arduinosense.db";	// Naam van je DB
    public static final int dbVersion = 1;				// Versie nr van je db.

    public DatabaseHelper(Context ctx) {				// De constructor doet niet veel meer dan ...
        super(ctx, dbName, null, dbVersion);			// … de super constructor aan te roepen.
    }

    public static synchronized DatabaseHelper getHelper (Context ctx){  // SYNCRONIZED TRUC
        if (mInstance == null){
            mInstance = new DatabaseHelper(ctx);
            mSQLDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    public static synchronized DatabaseHelper getInstance(){
        return mInstance;
    }

    @Override // CREATE TABLE course (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, ects TEXT, code TEXT grade TEXT);
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseInfo.ProjectTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.ProjectTable.NAME + " TEXT," +
                        DatabaseInfo.ProjectTable.BTDEVICE + " INTEGER," +
                        DatabaseInfo.ProjectTable.MODULES + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.BTControllerTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.BTControllerTable.NAME + " TEXT," +
                        DatabaseInfo.BTControllerTable.MAC + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.ModuleTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.ModuleTable.NAME + " TEXT," +
                        DatabaseInfo.ModuleTable.X_POS + " INTEGER," +
                        DatabaseInfo.ModuleTable.Y_POS + " INTEGER," +
                        DatabaseInfo.ModuleTable.ADDRESS + " INTEGER," +
                        DatabaseInfo.ModuleTable.IO + " INTEGER," +
                        DatabaseInfo.ModuleTable.VALUE + " INTEGER," +
                        DatabaseInfo.ModuleTable.CLASS + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.ButtonTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.ButtonTable.NAME + " TEXT," +
                        DatabaseInfo.ButtonTable.X_POS + " INTEGER," +
                        DatabaseInfo.ButtonTable.Y_POS + " INTEGER," +
                        DatabaseInfo.ButtonTable.ADDRESS + " INTEGER," +
                        DatabaseInfo.ButtonTable.IO + " INTEGER," +
                        DatabaseInfo.ButtonTable.VALUE + " INTEGER," +
                        DatabaseInfo.ButtonTable.STATE + " INTEGER," +
                        DatabaseInfo.ButtonTable.TOGGLE + " INTEGER," +
                        DatabaseInfo.ButtonTable.CLASS + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.ConsoleTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.ConsoleTable.NAME + " TEXT," +
                        DatabaseInfo.ConsoleTable.X_POS + " INTEGER," +
                        DatabaseInfo.ConsoleTable.Y_POS + " INTEGER," +
                        DatabaseInfo.ConsoleTable.ADDRESS + " INTEGER," +
                        DatabaseInfo.ConsoleTable.IO + " INTEGER," +
                        DatabaseInfo.ConsoleTable.VALUE + " INTEGER," +
                        DatabaseInfo.ConsoleTable.COMMANDS + " TEXT," +
                        DatabaseInfo.ConsoleTable.LOGGING + " TEXT," +
                        DatabaseInfo.ConsoleTable.SHOWDETAIL + " INTEGER," +
                        DatabaseInfo.ConsoleTable.CLASS + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.SliderTable.TABLE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.SliderTable.NAME + " TEXT," +
                        DatabaseInfo.SliderTable.X_POS + " INTEGER," +
                        DatabaseInfo.SliderTable.Y_POS + " INTEGER," +
                        DatabaseInfo.SliderTable.ADDRESS + " INTEGER," +
                        DatabaseInfo.SliderTable.IO + " INTEGER," +
                        DatabaseInfo.SliderTable.VALUE + " INTEGER," +
                        DatabaseInfo.SliderTable.MIN_RANGE + " INTEGER," +
                        DatabaseInfo.SliderTable.MAX_RANGE + " INTEGER," +
                        DatabaseInfo.SliderTable.CLASS + " TEXT);"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {	 // BIJ EEN UPDATE VAN DE DB (ID verhoogd)
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.ProjectTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.BTControllerTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.ModuleTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.ButtonTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.ConsoleTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.SliderTable.TABLE);
        onCreate(db);									 // EN CREER HET OPNIEUW
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version ){
        super(context, name, factory, version);
    }

    public int insert(String table, String nullColumnHack, ContentValues values){
        long id = mSQLDB.insert(table, nullColumnHack, values);

        return (int)id;
    }

    public boolean update(String table, ContentValues values, String where){
        return mSQLDB.update(table, values, where, null) > 0 ;
    }

    public boolean remove(String table, String where){
        return mSQLDB.delete(table, where, null) > 0;
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectArgs, String groupBy, String having, String orderBy){
        return mSQLDB.query(table, columns, selection, selectArgs, groupBy, having, orderBy);
    }
}
