package arduinosense.arduinosense.database;

/**
 * Created by Yoxx on 29/03/16.
 */
public class DatabaseInfo {

    public class ProjectTable {
        public static final String TABLE = "projects";
        public static final String NAME = "name";
        public static final String BTDEVICE = "btdevice";
        public static final String MODULES = "modules";
    }

    public class BTControllerTable {
        public static final String TABLE = "bluetooth_devices";
        public static final String NAME = "name";
        public static final String MAC = "mac";
    }

    public class ModuleTable {
        public static final String TABLE = "modules";
        public static final String NAME = "name";
        public static final String X_POS = "x_pos";
        public static final String Y_POS = "y_pos";
        public static final String ADDRESS = "address";
        public static final String IO = "io";
        public static final String VALUE = "value";
        public static final String CLASS = "class";
    }

    public class ButtonTable {
        public static final String TABLE = "buttons";
        public static final String NAME = "name";
        public static final String X_POS = "x_pos";
        public static final String Y_POS = "y_pos";
        public static final String ADDRESS = "address";
        public static final String IO = "io";
        public static final String VALUE = "value";
        public static final String STATE = "state";
        public static final String TOGGLE = "toggle";
        public static final String CLASS = "class";
    }

    public class ConsoleTable {
        public static final String TABLE = "consoles";
        public static final String NAME = "name";
        public static final String X_POS = "x_pos";
        public static final String Y_POS = "y_pos";
        public static final String ADDRESS = "address";
        public static final String IO = "io";
        public static final String VALUE = "value";
        public static final String COMMANDS = "commands";
        public static final String LOGGING = "logging";
        public static final String SHOWDETAIL = "show_detail";
        public static final String CLASS = "class";
    }

    public class SliderTable {
        public static final String TABLE = "sliders";
        public static final String NAME = "name";
        public static final String X_POS = "x_pos";
        public static final String Y_POS = "y_pos";
        public static final String ADDRESS = "address";
        public static final String IO = "io";
        public static final String VALUE = "value";
        public static final String MIN_RANGE = "min_range";
        public static final String MAX_RANGE = "max_range";
        public static final String CLASS = "class";
    }
}
