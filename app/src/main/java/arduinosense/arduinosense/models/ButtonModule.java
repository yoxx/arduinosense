package arduinosense.arduinosense.models;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import java.util.ArrayList;
import arduinosense.arduinosense.R;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.utils.Utils;

/**
 * This Class creates a ButtonModule object that can be added as Module to a Project
 *
 * Created by Yoxx on 24/03/16.
 */
public class ButtonModule extends Module {
    /* Define variables */
    private boolean state;
    private boolean toggle;
    private transient Switch sw;
    private transient Button btn;

    /**
     * Create a standard module with:
     * default: name + (number_of_mudules + 1)
     * default: address = (number_of_mudules + 1)
     * default: Input = True
     *
     * @param count int
     */
    public ButtonModule(int count) {
        super(count, ButtonModule.class, count, Utils.OUTPUT);
        this.state = false; // Default OFF
        this.toggle = true; // Default Toggle
    }

    /**
     * ButtonModule constructor
     *
     * @param name    String
     * @param address int
     * @param io      boolean
     */
    public ButtonModule(String name, int address, boolean io) {
        super(name, address, io);
        this.state = false; // Default OFF
        this.toggle = true; // Default Toggle
        this.clazz = ButtonModule.class.toString();
    }

    /**
     * Returns the state of the button
     *
     * @return boolean
     */
    public boolean getState() {
        return this.state;
    }

    /**
     * Set the state of the button
     *
     * @param state boolean
     */
    public void setState(boolean state) {
        this.state = state;
    }

    /**
     * Return the mode of the button. This can be Toggle or constantly pressed
     *
     * @return boolean
     */
    public boolean isToggle() {
        return this.toggle;
    }

    /**
     * Set the Toggle mode of the button
     *
     * @param toggle boolean
     */
    public void setToggle(boolean toggle) {
        this.toggle = toggle;
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_" + this.name);
        t.start();
    }

    public Switch getSwitch(){
        return this.sw;
    }

    public Button getButton(){
        return this.btn;
    }

    @Override
    public void showValueInView(int address, int value){
        if (this.sw != null) {
            if (value == 1) {
                this.sw.setChecked(true);
            } else if (value == 0){
                this.sw.setChecked(false);
            }
        }
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.ButtonTable.NAME, this.name);
            values.put(DatabaseInfo.ButtonTable.X_POS, this.x_pos);
            values.put(DatabaseInfo.ButtonTable.Y_POS, this.y_pos);
            values.put(DatabaseInfo.ButtonTable.ADDRESS, this.address);
            values.put(DatabaseInfo.ButtonTable.IO, this.io);
            values.put(DatabaseInfo.ButtonTable.VALUE, this.value);
            values.put(DatabaseInfo.ButtonTable.STATE, this.state);
            values.put(DatabaseInfo.ButtonTable.TOGGLE, this.toggle);
            values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);

            int id = db.insert(DatabaseInfo.ButtonTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.ButtonTable.NAME, this.name);
        values.put(DatabaseInfo.ButtonTable.X_POS, this.x_pos);
        values.put(DatabaseInfo.ButtonTable.Y_POS, this.y_pos);
        values.put(DatabaseInfo.ButtonTable.ADDRESS, this.address);
        values.put(DatabaseInfo.ButtonTable.IO, this.io);
        values.put(DatabaseInfo.ButtonTable.VALUE, this.value);
        values.put(DatabaseInfo.ButtonTable.STATE, this.state);
        values.put(DatabaseInfo.ButtonTable.TOGGLE, this.toggle);
        values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);
        db.update(DatabaseInfo.ButtonTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.ButtonTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<ButtonModule> list = new ArrayList<ButtonModule>();
        Cursor cursor = db.query(DatabaseInfo.ButtonTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        while (cursor.moveToNext()) {

            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
            int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
            int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
            int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

            boolean io;
            if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
                io = Utils.INPUT;
            } else {
                io = Utils.OUTPUT;
            }

            boolean state;
            if (cursor.getInt(cursor.getColumnIndex("state")) == 1) {
                state = true;
            } else {
                state = false;
            }
            boolean toggle;
            if (cursor.getInt(cursor.getColumnIndex("toggle")) == 1) {
                toggle = true;
            } else {
                toggle = false;
            }

            ButtonModule buttonModule = new ButtonModule(name, address, io);
            buttonModule.setId(id);
            buttonModule.setClazz(clazz);
            buttonModule.setValue(value);
            buttonModule.setXpos(xpos);
            buttonModule.setYpos(ypos);
            buttonModule.setToggle(toggle);
            buttonModule.setState(state);

            list.add(buttonModule);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return ButtonModule
     */
    static public ButtonModule find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.ButtonTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
        int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
        int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
        int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

        boolean io;
        if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
            io = Utils.INPUT;
        } else {
            io = Utils.OUTPUT;
        }

        boolean state;
        if (cursor.getInt(cursor.getColumnIndex("state")) == 1) {
            state = true;
        } else {
            state = false;
        }
        boolean toggle;
        if (cursor.getInt(cursor.getColumnIndex("toggle")) == 1) {
            toggle = true;
        } else {
            toggle = false;
        }

        ButtonModule buttonModule = new ButtonModule(name, address, io);
        buttonModule.setId(id);
        buttonModule.setClazz(clazz);
        buttonModule.setValue(value);
        buttonModule.setXpos(xpos);
        buttonModule.setYpos(ypos);
        buttonModule.setToggle(toggle);
        buttonModule.setState(state);

        cursor.close();

        return buttonModule;
    }

    public void addToView(Context context, RelativeLayout v, final BluetoothDevice bluetoothDevice) {
        if (this.toggle) {
            final Button btn = new Button(context);
            btn.setId(this.id + 100);
            btn.setText(this.name);
            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            if (this.x_pos != 0 && this.y_pos != 0) {
                params2.leftMargin = this.x_pos;
                params2.topMargin = this.y_pos;
            }
            btn.setLayoutParams(params2);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            final ButtonModule btnMod = this;
            btn.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent e) {
                    if(!moveable) {
                        if (e.getAction() == MotionEvent.ACTION_MOVE) {
                            if (bluetoothDevice != null) {
                                bluetoothDevice.sendData(name, address, 1);
                            }
                        } else if (e.getAction() == MotionEvent.ACTION_UP || e.getAction() == MotionEvent.ACTION_CANCEL) {
                            if (bluetoothDevice != null) {
                                bluetoothDevice.sendData(name, address, 0);
                            }
                        }
                    } else {
                        if (e.getAction() == MotionEvent.ACTION_MOVE) {
                            ClipData data = ClipData.newPlainText("", "");
                            View.DragShadowBuilder shadow = new View.DragShadowBuilder(btn);
                            v.startDrag(data, shadow, btnMod, 0);
                        }
                    }
                    return false;
                }
            });

            super.StartOnDragListener(v);

            v.addView(btn);
            this.btn = btn;

        } else {
            // get the XML inflator
            LayoutInflater inflater = LayoutInflater.from(context);
            // Retrieve the layout
            final RelativeLayout module_layout = (RelativeLayout) inflater.inflate(R.layout.switch_module, null, false);
            // Set the layout parameters
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            // Add rules to the parameters
            if (this.x_pos != 0 && this.y_pos != 0) {
                params.leftMargin = this.x_pos;
                params.topMargin = this.y_pos;
            }
            // Add the parameters to the layout
            module_layout.setLayoutParams(params);
            // Add the layout to our root layout
            v.addView(module_layout);
            final Switch sw = (Switch) v.findViewById(R.id.switch1);
            sw.setId(this.id + 100);
            sw.setText(this.name);
            if (value == 1){
                sw.setChecked(true);
            }
            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        value = 1;
                        if (bluetoothDevice != null) {
                            bluetoothDevice.sendData(name, address, value);
                            save();
                        }
                    } else {
                        value = 0;
                        if (bluetoothDevice != null) {
                            bluetoothDevice.sendData(name, address, value);
                            save();
                        }
                    }
                }
            });

            final ButtonModule btnMod = this;
            sw.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(moveable) {
                        ClipData data = ClipData.newPlainText("", "");
                        View.DragShadowBuilder shadow = new View.DragShadowBuilder(sw);
                        v.startDrag(data, shadow, btnMod, 0);
                    }
                    return false;
                }
            });

            super.StartOnDragListener(v);

            this.sw = sw;
        }
    }
}
