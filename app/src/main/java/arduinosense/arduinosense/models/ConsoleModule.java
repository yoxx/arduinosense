package arduinosense.arduinosense.models;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import arduinosense.arduinosense.R;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.utils.Utils;

/**
 * This Class creates a ConsoleModule object that can be added as Module to a Project
 * <p/>
 * Created by Yoxx on 24/03/16.
 */
public class ConsoleModule extends Module {
    /* Define the variables for the class*/
    private ArrayList<String> commands;
    private ArrayList<String> logging;
    private boolean showDetail;
    private int linecount = 0;

    /**
     * Create a standard module with:
     * default: name + (number_of_mudules + 1)
     * default: address = (number_of_mudules + 1)
     * default: Input = True
     *
     * @param count int
     */
    public ConsoleModule(int count) {
        super(count, ConsoleModule.class, count, Utils.INPUT);
        this.commands = new ArrayList<String>();
        this.logging = new ArrayList<String>();
        this.showDetail = true;
    }

    /**
     * Constructor for this class
     *
     * @param name       String
     * @param address    int
     * @param io         boolean
     * @param showDetail boolean
     */
    public ConsoleModule(String name, int address, boolean io, boolean showDetail) {
        super(name, address, io);
        this.commands = new ArrayList<String>();
        this.logging = new ArrayList<String>();
        this.showDetail = showDetail;
        this.clazz = ConsoleModule.class.toString();
    }

    /**
     * gets the commands that are given to the arduino
     *
     * @return String
     */
    public ArrayList<String> getCommands() {
        return this.commands;
    }


    /**
     * Sets the commands that are given to the arduino
     */
    public void loadCommands() {
        for (int count = 0; count < this.logging.size(); count++) {
//            this.console_view.append(this.logging.get(count) + "\n");
        }
    }

    @Override
    public void showValueInView(int address, int value){
        addLogline("Recieved: {address, value} {" + address + ", " + value + "}");
    }

    /**
     * Sets the commands that are given to the arduino
     *
     * @param commands ArrayList
     */
    public void setCommands(ArrayList<String> commands) {
        this.commands = commands;
    }

    /**
     * Adds a command to the list and executes it
     *
     * @param command String
     */
    public void addCommand(String command) {
        if (this.view != null) {
            this.view.append("running:" + command + "\n");
         /* We should do something with the command here... */
            this.commands.add(command);
        }
    }

    /**
     * Gets the logs list
     *
     * @return ArrayList
     */
    public ArrayList<String> getLog() {
        return this.logging;
    }

    /**
     * Sets the log list
     *
     * @param log ArrayList
     */
    public void setLog(ArrayList<String> log) {
        this.logging = log;
    }

    /**
     * Adds a string to the list and shows it
     *
     * @param logline String
     */
    public void addLogline(String logline) {
        if (this.view != null) {
            if (linecount < 11) {
                this.view.append(logline + "\n");
                this.logging.add(logline);
                this.linecount += 1;
            } else {
                linecount = 0;
                this.view.setText(this.name + ":\n");
                this.view.append(logline + "\n");
                this.logging.add(logline);
                linecount += 1;
            }
        }
    }

    /**
     * Gets the boolean that shows if we should show logging
     *
     * @return boolean
     */
    public boolean getShowDetail() {
        return this.showDetail;
    }

    /**
     * Sets the boolean for the detail
     *
     * @param detail boolean
     */
    public void setShowDetail(boolean detail) {
        this.showDetail = detail;
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_" + this.name);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database in a seperate Thread
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            String jsonCommands = Utils.arrayListToJsonString(this.commands);
            String jsonLogging = Utils.arrayListToJsonString(this.logging);

            values.put(DatabaseInfo.ConsoleTable.NAME, this.name);
            values.put(DatabaseInfo.ConsoleTable.X_POS, this.x_pos);
            values.put(DatabaseInfo.ConsoleTable.Y_POS, this.y_pos);
            values.put(DatabaseInfo.ConsoleTable.ADDRESS, this.address);
            values.put(DatabaseInfo.ConsoleTable.IO, this.io);
            values.put(DatabaseInfo.ConsoleTable.VALUE, this.value);
            values.put(DatabaseInfo.ConsoleTable.COMMANDS, jsonCommands);
            values.put(DatabaseInfo.ConsoleTable.LOGGING, jsonLogging);
            values.put(DatabaseInfo.ConsoleTable.SHOWDETAIL, this.showDetail);
            values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);

            int id = db.insert(DatabaseInfo.ConsoleTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();
        String jsonCommands = Utils.arrayListToJsonString(this.commands);
        String jsonLogging = Utils.arrayListToJsonString(this.logging);

        values.put(DatabaseInfo.ConsoleTable.NAME, this.name);
        values.put(DatabaseInfo.ConsoleTable.X_POS, this.x_pos);
        values.put(DatabaseInfo.ConsoleTable.Y_POS, this.y_pos);
        values.put(DatabaseInfo.ConsoleTable.ADDRESS, this.address);
        values.put(DatabaseInfo.ConsoleTable.IO, this.io);
        values.put(DatabaseInfo.ConsoleTable.VALUE, this.value);
        values.put(DatabaseInfo.ConsoleTable.COMMANDS, jsonCommands);
        values.put(DatabaseInfo.ConsoleTable.LOGGING, jsonLogging);
        values.put(DatabaseInfo.ConsoleTable.SHOWDETAIL, this.showDetail);
        values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);
        db.update(DatabaseInfo.ConsoleTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.ConsoleTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<ConsoleModule> list = new ArrayList<ConsoleModule>();
        Cursor cursor = db.query(DatabaseInfo.ConsoleTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        while (cursor.moveToNext()) {

            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
            int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
            int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
            int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));
            String commands = (String) cursor.getString(cursor.getColumnIndex("commands"));
            String Logging = (String) cursor.getString(cursor.getColumnIndex("logging"));

            boolean io;
            if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
                io = Utils.INPUT;
            } else {
                io = Utils.OUTPUT;
            }

            boolean showDetail;
            if (cursor.getInt(cursor.getColumnIndex("show_detail")) == 1) {
                showDetail = true;
            } else {
                showDetail = false;
            }


            ConsoleModule consoleModule = new ConsoleModule(name, address, io, showDetail);
            consoleModule.setId(id);
            consoleModule.setClazz(clazz);
            consoleModule.setValue(value);
            consoleModule.setXpos(xpos);
            consoleModule.setYpos(ypos);
            consoleModule.setCommands(Utils.jsonStringToArrayListString(commands));
            consoleModule.setLog(Utils.jsonStringToArrayListString(Logging));

            list.add(consoleModule);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return ConsoleModule
     */
    static public ConsoleModule find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.ConsoleTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
        int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
        int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
        int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));
        String commands = (String) cursor.getString(cursor.getColumnIndex("commands"));
        String Logging = (String) cursor.getString(cursor.getColumnIndex("logging"));

        boolean io;
        if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
            io = Utils.INPUT;
        } else {
            io = Utils.OUTPUT;
        }

        boolean showDetail;
        if (cursor.getInt(cursor.getColumnIndex("show_detail")) == 1) {
            showDetail = true;
        } else {
            showDetail = false;
        }

        ConsoleModule consoleModule = new ConsoleModule(name, address, io, showDetail);
        consoleModule.setId(id);
        consoleModule.setClazz(clazz);
        consoleModule.setValue(value);
        consoleModule.setXpos(xpos);
        consoleModule.setYpos(ypos);
        consoleModule.setCommands(Utils.jsonStringToArrayListString(commands));
        consoleModule.setLog(Utils.jsonStringToArrayListString(Logging));

        cursor.close();

        return consoleModule;
    }

    public void addToView(Context context, RelativeLayout v,final BluetoothDevice bluetoothDevice) {
        // get the XML inflator
        LayoutInflater inflater = LayoutInflater.from(context);
        // Retrieve the layout
        final RelativeLayout module_layout = (RelativeLayout) inflater.inflate(R.layout.console_module, (ViewGroup) v.getParent(), false);
        module_layout.setId(this.id + 200);
        // Set the params
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (this.x_pos != 0 && this.y_pos != 0) {
            params.leftMargin = this.x_pos;
            params.topMargin = this.y_pos;
        }
        // Add the parameters to the layout
        module_layout.setLayoutParams(params);
        // Add the layout to our root layout
        v.addView(module_layout);

        final EditText input_field = (EditText) v.findViewById(R.id.input_field);
        input_field.setId(this.id + 220);
        TextView console_view = (TextView) v.findViewById(R.id.console_field);
        console_view.setId(this.id + 240);
        console_view.setText(name + ":\n");

        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params2.addRule(RelativeLayout.BELOW, console_view.getId());
        input_field.setLayoutParams(params2);

        input_field.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        String command= v.getText().toString();
                        addCommand(command);
                        if(bluetoothDevice != null) {
                            bluetoothDevice.sendStringData(name, command);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        final ConsoleModule consoleMod = this;
        module_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(moveable) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadow = new View.DragShadowBuilder(module_layout);
                    v.startDrag(data, shadow, consoleMod, 0);
                }
                return false;
            }
        });

        super.StartOnDragListener(v);

        this.view = console_view;
        this.layout = module_layout;
    }
}
