package arduinosense.arduinosense.models;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import arduinosense.arduinosense.R;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.utils.Utils;

/**
 * This Class creates a Module object that can be added to a Project
 *
 * Created by Yoxx on 24/03/16.
 */
public class Module implements Serializable, Runnable {
    /* Define variables protected so we can use them in other classes */
    protected int id;
    protected String name;
    protected int x_pos;
    protected int y_pos;
    protected int address;
    protected boolean io;
    protected int value;
    protected String clazz;
    protected BluetoothDevice bluetoothDevice;
    protected transient TextView view;
    protected transient RelativeLayout layout;
    protected boolean moveable;

    /**
     * Default Constructor we can override when inhertenting this class
     */
    protected Module() {
    }

    /**
     * Constructor of a module
     *
     * @param name    String
     * @param address int
     * @param io      boolean
     */
    public Module(String name, int address, boolean io) {
        this.name = name;
        this.x_pos = 150;
        this.y_pos = 150;
        this.address = address;
        this.io = io;
        this.value = 0;
        this.clazz = Module.class.toString();
    }

    public Module(int count, Class c, int address, boolean io) {
        count = count + 1;
        switch (c.getName()) {
            case "arduinosense.arduinosense.models.Module":
                this.name = "Module" + count;
                break;
            case "arduinosense.arduinosense.models.ConsoleModule":
                this.name = "Console" + count;
                break;
            case "arduinosense.arduinosense.models.SliderModule":
                this.name = "Slider" + count;
                break;
            case "arduinosense.arduinosense.models.ButtonModule":
                this.name = "Button" + count;
                break;
        }

        this.clazz = c.getName();
        this.x_pos = 150;
        this.y_pos = 150;
        this.address = address;
        this.io = io;
        this.value = 0;
    }

    /**
     * Returns the id of the object
     *
     * @return int
     */
    public int getId() {
        return this.id;
    }

    /**
     * Sets the id of the module
     *
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Return the name of this module
     *
     * @return String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this module
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the X-position of this module
     *
     * @return int
     */
    public int getXpos() {
        return this.x_pos;
    }

    /**
     * Return the Y-position of this module
     *
     * @return int
     */
    public int getYpos() {
        return this.y_pos;
    }

    /**
     * Set the X-position of this module
     *
     * @param xpos int
     */
    public void setXpos(int xpos) {
        this.x_pos = xpos;
    }

    /**
     * Set the Y-position of this module
     *
     * @param ypos int
     */
    public void setYpos(int ypos) {
        this.y_pos = ypos;
    }

    /**
     * Return the address of the module to the arduino
     *
     * @return int
     */
    public int getAddress() {
        return this.address;
    }

    /**
     * Set the address of the module
     *
     * @param address int
     */
    public void setAddress(int address) {
        this.address = address;
    }

    /**
     * Return if the module is an input or an output module
     *
     * @return boolean
     */
    public boolean getIO() {
        return this.io;
    }

    /**
     * Set if the module is an input or an output module
     *
     * @param io boolean
     */
    public void setIO(boolean io) {
        this.io = io;
    }

    /**
     * Return the current value of the module
     *
     * @return int
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Set the current value of this module
     *
     * @param value int
     */
    public void setValue(int value) {
        this.value = value;
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public boolean getMoveable() {
        return this.moveable;
    }

    public void showValueInView(int address, int value) {
        if (this.value != value) {
            this.value = value;
            this.view.setText("" + value);
            save();
        }
    }

    /**
     * Set the bluetoothcontroller where we can send data with
     *
     * @param bluetoothDevice BluetoothDevice
     */
    public void setBTController(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    /**
     * returns the bluetoothcontroller we use to send data with
     *
     * @return BluetoothDevice
     */
    public BluetoothDevice getBTController() {
        return this.bluetoothDevice;
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_" + this.name);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.ModuleTable.NAME, this.name);
            values.put(DatabaseInfo.ModuleTable.X_POS, this.x_pos);
            values.put(DatabaseInfo.ModuleTable.Y_POS, this.y_pos);
            values.put(DatabaseInfo.ModuleTable.ADDRESS, this.address);
            values.put(DatabaseInfo.ModuleTable.IO, this.io);
            values.put(DatabaseInfo.ModuleTable.VALUE, this.value);
            values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);

            int id = db.insert(DatabaseInfo.ModuleTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.ModuleTable.NAME, this.name);
        values.put(DatabaseInfo.ModuleTable.X_POS, this.x_pos);
        values.put(DatabaseInfo.ModuleTable.Y_POS, this.y_pos);
        values.put(DatabaseInfo.ModuleTable.ADDRESS, this.address);
        values.put(DatabaseInfo.ModuleTable.IO, this.io);
        values.put(DatabaseInfo.ModuleTable.VALUE, this.value);
        values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);
        db.update(DatabaseInfo.ModuleTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.ModuleTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<Module> list = new ArrayList<Module>();
        Cursor cursor = db.query(DatabaseInfo.ModuleTable.TABLE, new String[]{"*"}, where, null, null, null, null);
        while (cursor.moveToNext()) {

            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
            int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
            int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
            int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

            boolean io;
            if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
                io = Utils.INPUT;
            } else {
                io = Utils.OUTPUT;
            }

            Module module = new Module(name, address, io);
            module.setId(id);
            module.setValue(value);
            module.setXpos(xpos);
            module.setYpos(ypos);
            module.setClazz(clazz);

            list.add(module);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return Module
     */
    static public Module find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.ModuleTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
        int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
        int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
        int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

        boolean io;
        if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
            io = Utils.INPUT;
        } else {
            io = Utils.OUTPUT;
        }

        Module module = new Module(name, address, io);
        module.setId(id);
        module.setValue(value);
        module.setXpos(xpos);
        module.setYpos(ypos);
        module.setClazz(clazz);

        cursor.close();

        return module;
    }

    public String getClazz() {
        return this.clazz;
    }

    public void setClazz(String clazz){
        this.clazz = clazz;
    }

    public RelativeLayout getLayout(){
        return this.layout;
    }

    public void addToView(Context context, RelativeLayout v, final BluetoothDevice bluetoothDevice) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (this.x_pos != 0 && this.y_pos != 0) {
            params.leftMargin = this.x_pos;
            params.topMargin = this.y_pos;
        }
        // get the XML inflator
        LayoutInflater inflater = LayoutInflater.from(context);
        // Retrieve the layout
        final RelativeLayout module_layout = (RelativeLayout) inflater.inflate(R.layout.edittext_module, (ViewGroup) v.getParent(), false);
        // Add the parameters to the layout
        module_layout.setLayoutParams(params);
        // Add the layout to our root layout
        v.addView(module_layout);
        EditText text = (EditText) v.findViewById(R.id.range);
        TextView text_name = (TextView) v.findViewById(R.id.text_range);
        text_name.setId(id + 1);
        text_name.setText(name);
        text.setId(id);

        if (!getIO()) {
            text.setEnabled(false);
        } else {
            text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (event != null) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            value = Integer.parseInt(v.getText().toString());
                            if (bluetoothDevice != null) {
                                bluetoothDevice.sendData(name, address, value);
                                save();
                            }
                            return true;
                        }
                    }
                    return false;
                }
            });
        }
        text.setImeActionLabel("Send", KeyEvent.KEYCODE_ENTER);
        ;
        text.setText("" + this.value);

        module_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (moveable) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadow = new View.DragShadowBuilder(module_layout);
                    v.startDrag(data, shadow, this, 0);
                }
                return false;
            }
        });

        StartOnDragListener(v);

        this.view = text;
        this.layout = module_layout;
    }

    public void StartOnDragListener(RelativeLayout baseLayout) {
        baseLayout.setOnDragListener(new View.OnDragListener() {
                                         @Override
                                         public boolean onDrag(View v, DragEvent event) {
                                             RelativeLayout.LayoutParams params;
                                             Module tmp = (Module)event.getLocalState();
                                             switch (event.getAction()) {
                                                 case DragEvent.ACTION_DRAG_STARTED:
                                                     break;
                                                 case DragEvent.ACTION_DRAG_ENTERED:
                                                     break;
                                                 case DragEvent.ACTION_DRAG_EXITED:
                                                     break;
                                                 case DragEvent.ACTION_DRAG_LOCATION:
                                                         tmp.setXpos((int) event.getX());
                                                         tmp.setYpos((int) event.getY());
                                                     break;
                                                 case DragEvent.ACTION_DRAG_ENDED:
                                                     break;
                                                 case DragEvent.ACTION_DROP:
                                                     if (tmp instanceof ButtonModule) {
                                                         ButtonModule btn = (ButtonModule) tmp;
                                                         if (btn.getSwitch() != null) {
                                                             Switch sw = btn.getSwitch();
                                                             params = (RelativeLayout.LayoutParams) sw.getLayoutParams();
                                                             params.leftMargin = btn.getXpos();
                                                             params.topMargin = btn.getYpos();
                                                             sw.setLayoutParams(params);
                                                         } else {
                                                             Button sw = btn.getButton();
                                                             params = (RelativeLayout.LayoutParams) sw.getLayoutParams();
                                                             params.leftMargin = btn.getXpos();
                                                             params.topMargin = btn.getYpos();
                                                             sw.setLayoutParams(params);
                                                         }
                                                         btn.save();
                                                     } else {
                                                         RelativeLayout sw = (RelativeLayout) tmp.getLayout();
                                                         params = (RelativeLayout.LayoutParams) sw.getLayoutParams();
                                                         params.leftMargin = tmp.getXpos();
                                                         params.topMargin = tmp.getYpos();
                                                         sw.setLayoutParams(params);
                                                         tmp.save();
                                                     }
                                                     break;
                                                 default:
                                                     break;
                                             }
                                             return true;
                                         }
                                     }

        );
    }

    /**
     * Returns the id name and bluetooth device it is using for the arraylist
     *
     * @return String
     */
    @Override
    public String toString() {
        String cl = "";
        switch (this.getClazz()){
            case "arduinosense.arduinosense.models.Module":
                cl = "Module";
                break;
            case "arduinosense.arduinosense.models.ButtonModule":
                cl = "Button";
                break;
            case "arduinosense.arduinosense.models.SliderModule":
                cl = "Slider";
                break;
            case "arduinosense.arduinosense.models.ConsoleModule":
                cl = "Console";
                break;
        }
        return cl + " name: " + name + " address: " + address;
    }
}

