package arduinosense.arduinosense.models;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.util.ArrayList;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.utils.Utils;

/**
 * This Class allows a Project object to be created where we can add Module objects
 *
 * Created by Yoxx on 24/03/16.
 */
public class Project implements Serializable, Runnable {
    /* Define the variables we need */
    private int id;
    private String name;
    private BluetoothDevice bluetoothDevice;
    private ArrayList<Module> modules;
    private boolean moveable;

    /**
     * Project Constructor
     *
     * @param name String
     * @param bluetoothDevice BluetoothDevice
     */
    public Project(String name, BluetoothDevice bluetoothDevice){
        this.name = name; // Should never be null
        this.bluetoothDevice = bluetoothDevice; // Can be null
        this.modules = new ArrayList<Module>();
    }

    /**
     * Returns the id of the object
     *
     * @return int
     */
    public int getId(){
        return this.id;
    }

    /**
     * Sets the id of the module
     *
     * @param id int
     */
    public void setId( int id){
        this.id = id;
    }

    /**
     * Return the name of the project
     * @return String
     */
    public String getName(){ return this.name; }

    /**
     * Set the name of the project
     * @param name String
     */
    public void setName(String name){ this.name = name; }

    /**
     * Return the Bluetooth controller
     * @return BluetoothDevice
     */
    public BluetoothDevice getBTController(){ return this.bluetoothDevice; }

    /**
     * Set the Bleutooth controller
     * @param controller BluetoothDevice
     */
    public void setBTController(BluetoothDevice controller){ this.bluetoothDevice = controller; }

    /**
     * Return the modules that belong to this project
     * @return ArrayList
     */
    public ArrayList<Module> getModules(){ return this.modules; }

    /**
     * Sets the modules of a project
     *
     * @param modules ArrayList
     */
    public void setModules (ArrayList<Module> modules){ this.modules = modules; }

    /**
     * Add a module to the module list
     * @param module Module
     */
    public void addModule(Module module){ this.modules.add(module); }

    /**
     * Remove a module from the module list
     * @param module Module
     */
    public void removeModule(Module module){ this.modules.remove(module); }

    public void setMoveable(boolean moveable){
        this.moveable = moveable;
    }

    public boolean getMoveable() {
        return this.moveable;
    }

    public void saveAsThread(){
        Thread t = new Thread(this,"save_" + this.name);
        t.start();
    }

    @Override
    public void run() {
        update();
        return;
    }

    /**
     * Saves the object to the database
     *
     */
    public void save(){

        if (this.id == 0){
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();
        String jsonModules = Utils.arrayListToJsonString(this.modules);

        values.put(DatabaseInfo.ProjectTable.NAME, this.name);
        if (bluetoothDevice != null) {
            values.put(DatabaseInfo.ProjectTable.BTDEVICE, this.bluetoothDevice.getId());
        } else {
            values.put(DatabaseInfo.ProjectTable.BTDEVICE, 0);
        }
        values.put(DatabaseInfo.ProjectTable.MODULES, jsonModules);

            int id = db.insert(DatabaseInfo.ProjectTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();
        String jsonModules = Utils.arrayListToJsonString(this.modules);

        values.put(DatabaseInfo.ProjectTable.NAME, this.name);
        if (bluetoothDevice != null) {
            values.put(DatabaseInfo.ProjectTable.BTDEVICE, this.bluetoothDevice.getId());
        } else {
            values.put(DatabaseInfo.ProjectTable.BTDEVICE, 0);
        }
        values.put(DatabaseInfo.ProjectTable.MODULES, jsonModules);
        db.update(DatabaseInfo.ProjectTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete(){
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.ProjectTable.TABLE ,"_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList<Project> where(String where){
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<Project> list = new ArrayList<Project>();
        Cursor cursor = db.query(DatabaseInfo.ProjectTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        while(cursor.moveToNext()) {
            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int device_id = (int) cursor.getInt(cursor.getColumnIndex("btdevice"));
            String modules = (String)cursor.getString(cursor.getColumnIndex("modules"));

            BluetoothDevice bluetoothDevice;
            if (device_id != 0) {
                bluetoothDevice = BluetoothDevice.find("_id = " + device_id);
            } else {
                bluetoothDevice = null;
            }

            Project project = new Project(name, bluetoothDevice);
            project.setId(id);
            project.setModules(Utils.jsonStringToArrayListModule(modules, bluetoothDevice));

            list.add(project);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     * @return ArrayList
     */
    static public ArrayList<Project> where(){
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return Project
     */
    static public Project find(String where){
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.ProjectTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        int device_id = (int) cursor.getInt(cursor.getColumnIndex("btdevice"));
        String modules = (String)cursor.getString(cursor.getColumnIndex("modules"));

        BluetoothDevice bluetoothDevice;
        if (device_id != 0) {
            bluetoothDevice = BluetoothDevice.find("_id = " + device_id);
        } else {
            bluetoothDevice = null;
        }

        Project project = new Project(name, bluetoothDevice);
        project.setId(id);
        project.setModules(Utils.jsonStringToArrayListModule(modules, bluetoothDevice));

        cursor.close();

        return project;
    }

    /**
     * Counts the occurences of the module and returns them
     *
     * @param clazz Class
     * @return int
     */
    public int countModule(Class<?> clazz){
        int occurences = 0;
        for (int i = 0; i < this.modules.size(); i++){
            Class c = this.modules.get(i).getClass();
            if (clazz.isInstance(this.modules.get(i))){
                occurences += 1;
            }
        }
        return occurences;
    }

    /**
     * Returns the id name and bluetooth device it is using for the arraylist
     *
     * @return String
     */
    @Override
    public String toString() {
        String deviceName;
        if (this.bluetoothDevice != null) {
            deviceName = this.bluetoothDevice.getDeviceName();
        } else {
            deviceName = "no device";
        }
        return "Project " + this.name + " using " + deviceName;
    }
}
