package arduinosense.arduinosense.models;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.ArrayList;
import arduinosense.arduinosense.R;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.database.DatabaseHelper;
import arduinosense.arduinosense.database.DatabaseInfo;
import arduinosense.arduinosense.utils.Utils;

/**
 * This Class creates a SliderModule object that can be added as Module to a Project
 * <p/>
 * Created by Yoxx on 24/03/16.
 */
public class SliderModule extends Module {
    /* Define variables */
    private int minRange;
    private int maxRange;
    private transient SeekBar slider;

    /**
     * Create a standard module with:
     * default: name + (number_of_mudules + 1)
     * default: address = (number_of_mudules + 1)
     * default: Input = True
     *
     * @param count int
     */
    public SliderModule(int count) {
        super(count, SliderModule.class, count, Utils.OUTPUT);
        this.minRange = 0;
        this.maxRange = 100;
    }

    /**
     * SliderModule Constructor
     *
     * @param name     String
     * @param address  int
     * @param io       boolean
     * @param minRange int
     * @param maxRange int
     */
    public SliderModule(String name, int address, boolean io, int minRange, int maxRange) {
        super(name, address, io);
        this.minRange = minRange;
        this.maxRange = maxRange;
        this.clazz = SliderModule.class.toString();
    }

    @Override
    public void showValueInView(int address, int value){
        if (this.slider != null) {
            this.slider.setProgress(value);
        }
    }

    /**
     * Return the minimum range the slider is allowed to go
     *
     * @return int
     */
    public int getMinRange() {
        return this.minRange;
    }

    /**
     * Set the minimum range of the slider
     *
     * @param range int
     */
    public void setMinRange(int range) {
        this.minRange = range;
    }

    /**
     * Return the maximum range the slider is allowed to go
     *
     * @return int
     */
    public int getMaxRange() {
        return this.maxRange;
    }

    /**
     * Set the maximum range the slider is allowed to go
     *
     * @param range int
     */
    public void setMaxRange(int range) {
        this.maxRange = range;
    }


    private void saveAsThread() {
        Thread t = new Thread(this, "save_" + this.name);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.SliderTable.NAME, this.name);
            values.put(DatabaseInfo.SliderTable.X_POS, this.x_pos);
            values.put(DatabaseInfo.SliderTable.Y_POS, this.y_pos);
            values.put(DatabaseInfo.SliderTable.ADDRESS, this.address);
            values.put(DatabaseInfo.SliderTable.IO, this.io);
            values.put(DatabaseInfo.SliderTable.VALUE, this.value);
            values.put(DatabaseInfo.SliderTable.MIN_RANGE, this.minRange);
            values.put(DatabaseInfo.SliderTable.MAX_RANGE, this.maxRange);
            values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);

            int id = db.insert(DatabaseInfo.SliderTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.SliderTable.NAME, this.name);
        values.put(DatabaseInfo.SliderTable.X_POS, this.x_pos);
        values.put(DatabaseInfo.SliderTable.Y_POS, this.y_pos);
        values.put(DatabaseInfo.SliderTable.ADDRESS, this.address);
        values.put(DatabaseInfo.SliderTable.IO, this.io);
        values.put(DatabaseInfo.SliderTable.VALUE, this.value);
        values.put(DatabaseInfo.SliderTable.MIN_RANGE, this.minRange);
        values.put(DatabaseInfo.SliderTable.MAX_RANGE, this.maxRange);
        values.put(DatabaseInfo.ModuleTable.CLASS, this.clazz);
        db.update(DatabaseInfo.SliderTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.SliderTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<SliderModule> list = new ArrayList<SliderModule>();
        Cursor cursor = db.query(DatabaseInfo.SliderTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        while (cursor.moveToNext()) {

            String name = (String) cursor.getString(cursor.getColumnIndex("name"));
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
            int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
            int max_range = (int) cursor.getInt(cursor.getColumnIndex("max_range"));
            int minrange = (int) cursor.getInt(cursor.getColumnIndex("min_range"));
            int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
            int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
            int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

            boolean io;
            if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
                io = Utils.INPUT;
            } else {
                io = Utils.OUTPUT;
            }

            SliderModule slider = new SliderModule(name, address, io, minrange, max_range);
            slider.setId(id);
            slider.setClazz(clazz);
            slider.setValue(value);
            slider.setXpos(xpos);
            slider.setYpos(ypos);

            list.add(slider);
        }

        cursor.close();
        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return SliderModule
     */
    static public SliderModule find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.SliderTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        String clazz = (String) cursor.getString(cursor.getColumnIndex("class"));
        int address = (int) cursor.getInt(cursor.getColumnIndex("address"));
        int max_range = (int) cursor.getInt(cursor.getColumnIndex("max_range"));
        int minrange = (int) cursor.getInt(cursor.getColumnIndex("min_range"));
        int value = (int) cursor.getInt(cursor.getColumnIndex("value"));
        int xpos = (int) cursor.getInt(cursor.getColumnIndex("x_pos"));
        int ypos = (int) cursor.getInt(cursor.getColumnIndex("y_pos"));

        boolean io;
        if (cursor.getInt(cursor.getColumnIndex("io")) == 0) {
            io = Utils.INPUT;
        } else {
            io = Utils.OUTPUT;
        }

        SliderModule slider = new SliderModule(name, address, io, minrange, max_range);
        slider.setId(id);
        slider.setClazz(clazz);
        slider.setValue(value);
        slider.setXpos(xpos);
        slider.setYpos(ypos);

        cursor.close();
        return slider;
    }

    public void addToView(Context context, RelativeLayout v, final BluetoothDevice bluetoothDevice) {
        // get the XML inflator
        LayoutInflater inflater = LayoutInflater.from(context);
        // Retrieve the layout
        final RelativeLayout module_layout = (RelativeLayout) inflater.inflate(R.layout.slider_module, (ViewGroup) v.getParent() , false);

        module_layout.setId(this.id + 300);
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (this.x_pos != 0 && this.y_pos != 0) {
            params2.leftMargin = this.x_pos;
            params2.topMargin = this.y_pos;
        }
        // Add the parameters to the layout
        module_layout.setLayoutParams(params2);
        // Add the layout to our root layout
        v.addView(module_layout);
        TextView name_view = (TextView) module_layout.findViewById(R.id.name_view);
        name_view.setId(this.id + 320);
        name_view.setText(name);

        final TextView value_view = (TextView) module_layout.findViewById(R.id.value_view);
        value_view.setId(this.id + 340);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params3.addRule(RelativeLayout.BELOW, name_view.getId());
        final SeekBar slider = (SeekBar) module_layout.findViewById(R.id.seekBar);
        slider.setId(this.id + 360);
        slider.setLayoutParams(params3);

        params.setMargins(500, 0, 0, 0);
        value_view.setLayoutParams(params);

        final double difference = maxRange - minRange;
        final double step = difference / 100;
        double tmpVal;

        if(this.value < this.minRange){
            this.value = this.minRange;
            tmpVal = 0;
        } else if (this.value > this.maxRange){
            this.value = this.maxRange;
            tmpVal = 100;
        } else {
            tmpVal = ((this.value - this.minRange) / step);
        }
        slider.setProgress((int)tmpVal);

        value_view.setText("" + this.value);

        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(!moveable) {
                    double tmpStep;
                    if (seekBar.getProgress() == 0) {
                        tmpStep = 0;
                    } else {
                        tmpStep = step * (double) seekBar.getProgress();
                    }
                    int newValue = minRange + (int) tmpStep;

                    value_view.setText("" + newValue);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(!moveable) {
                    double tmpStep;
                    if (seekBar.getProgress() == 0) {
                        tmpStep = 0;
                    } else {
                        tmpStep = step * (double) seekBar.getProgress();
                    }
                    value = (int) (minRange + tmpStep);
                    if (bluetoothDevice != null) {
                        bluetoothDevice.sendData(name, address, value);
                        save();
                    }
                }
            }
        });

        final SliderModule sliderMod = this;
        module_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(moveable) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadow = new View.DragShadowBuilder(module_layout);
                    v.startDrag(data, shadow, sliderMod, 0);
                }
                return false;
            }
        });

        super.StartOnDragListener(v);

        this.slider = slider;
        this.layout = module_layout;
    }
}
