package arduinosense.arduinosense.utils;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import arduinosense.arduinosense.R;
import arduinosense.arduinosense.bluetooth.BluetoothDevice;
import arduinosense.arduinosense.models.ButtonModule;
import arduinosense.arduinosense.models.ConsoleModule;
import arduinosense.arduinosense.models.Module;
import arduinosense.arduinosense.models.SliderModule;

/**
 * Created by Yoxx on 29/03/16.
 */
public class Utils {
    /* Define input and output finals*/
    static final public boolean INPUT = false;
    static final public boolean OUTPUT = true;

    static public String arrayListToJsonString(ArrayList items) {
        Gson gson = new Gson();

        return gson.toJson(items);
    }

    static public ArrayList<String> jsonStringToArrayListString(String jsonString) {
        Gson gson = new Gson();

        String[] sArray = gson.fromJson(jsonString, String[].class);
        return new ArrayList<String>(Arrays.asList(sArray));
    }

    static public ArrayList<Module> jsonStringToArrayListModule(String jsonString, BluetoothDevice bluetoothDevice) {
        Gson gson = new Gson();
        ArrayList<Module> list = new ArrayList<Module>();

        Module[] mArray = gson.fromJson(jsonString, Module[].class);
        for (int count = 0; count < mArray.length; count++){
            String c = mArray[count].getClazz();
            switch (c){
                case "arduinosense.arduinosense.models.Module":
                    Module module = Module.find("_id = " + mArray[count].getId());
                    module.setBTController(bluetoothDevice);
                    list.add(module);
                    break;
                case "arduinosense.arduinosense.models.ButtonModule":
                    ButtonModule btn = ButtonModule.find("_id = " + mArray[count].getId());
                    btn.setBTController(bluetoothDevice);
                    list.add(btn);
                    break;
                case "arduinosense.arduinosense.models.SliderModule":
                    SliderModule slider = SliderModule.find("_id = " + mArray[count].getId());
                    slider.setBTController(bluetoothDevice);
                    list.add(slider);
                    break;
                case "arduinosense.arduinosense.models.ConsoleModule":
                    ConsoleModule console = ConsoleModule.find("_id = " + mArray[count].getId());
                    console.setBTController(bluetoothDevice);
                    list.add(console);
                    break;
            }
        }
        return list;
    }

    static public EditText addEditTextToView(Context context, RelativeLayout layout, String name, RelativeLayout.LayoutParams params, int id){
        // get the XML inflator
        LayoutInflater inflater = LayoutInflater.from(context);
        // Retrieve the layout
        RelativeLayout module_layout = (RelativeLayout) inflater.inflate(R.layout.edittext_module, (ViewGroup) layout.getParent() , false);
        // Add the parameters to the layout
        module_layout.setLayoutParams(params);
        // Add the layout to our root layout
        layout.addView(module_layout);
        EditText text = (EditText) layout.findViewById(R.id.range);
        TextView text_name = (TextView) layout.findViewById(R.id.text_range);
        text_name.setId(id+1);
        text_name.setText(name);
        text.setId(id);
        return text;
    }
}
